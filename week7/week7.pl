use strict;
use warnings;

my $str = 12345;
print "Original string: " .$str."\n";
print "Length of the string : ".length($str)."\n";

 print "Reversed strings " . reverse($str)."\n";

print "array of gene names\n";
my @genes = ("HOXB1", "ALPK1", "TP53");
my $size = scalar @genes;
print "A list of $size genes: @genes\n";
@genes = reverse @genes;
print "Reversed list of $size genes: @genes\n";
@genes = sort @genes;
print "Sorted list of $size genes: @genes\n\n";


print "Appending to an array\n";
@genes = ("HOXB1", "ALPK1", "TP53");
push @genes, "ZZZ3";
$size = scalar @genes;
print "There are now $size genes: @genes\n";
push @genes, ("EGF", "EFGR");
$size = scalar @genes;
print "There are now $size genes: @genes\n\n";


print "Removing items from end of array\n";
@genes = ("HOXB1", "ALPK1", "TP53", "EGF");
$size = scalar @genes;
print "A list of $size genes: @genes\n";
pop @genes;
$size = scalar @genes;
print "There are now $size genes: @genes\n";
my $gene = pop @genes;
$size = scalar @genes;
print "There are now $size genes: @genes\n";
print "There gene removed was $gene\n\n";

print "Removing items from front of array\n";
@genes = ("HOXB1", "ALPK1", "TP53", "EGF");
$size = scalar @genes;
print "A list of $size genes: @genes\n";
shift @genes;
$size = scalar @genes;
print "There are now $size genes: @genes\n";
$gene = shift @genes;
$size = scalar @genes;
print "There are now $size genes: @genes\n";
print "There gene removed was $gene\n\n";


unshift @genes , "HOXB1";
$size = scalar @genes;

print "there are now $size genes: @genes \n";

@genes = ("HOXB1", "ALPK1","TP53");
my $i = 0;
while($i < scalar @genes){
print "processing gene $genes[$i]\n";
$i++;

}


while (scalar @genes > 0) {
$gene = shift @genes;
print "Processing gene
$gene\n";
# put processing code here
}


my @names = ("Ali","Veli","Zeynep","Ayşe","Deniz");

foreach my $x (@names){
	print "$x\n";

}

print "another for loop to process a list\n";
@genes = ("HOXB1", "ALPK1", "TP53");
$size = scalar @genes;
for (my $i = 0; $i < $size; $i++) {
$gene = $genes[$i];
print "Processing gene $gene\n";
# put processing code here
}
$size = scalar @genes;
print "There are still $size genes in the list: @genes\n";

print "converting array to
string\n";
@genes = ("HOXB1", "ALPK1",
"TP53");
my $string = join(" ", @genes);
print "String of genes:
$string\n";
$size = length $string;
print "String has length:
$size\n";


